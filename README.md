# README #

### What is this repository for? ###

* Football Penalty Command Line game
* Version 1.0

# Pre-requirtes #
* Java 1.8
* Maven 3

### How do I get set up? ###

* Clone this repo https://chugh_sangam@bitbucket.org/chugh_sangam/football-penalty.git on your local machine
* Change serialization path in the file application.properties in resources folder. Just directory name only. No file name. 
* This is being used when user want to save game and play later.
* Navigate to project root directoty and Run this command "maven clean package". 
* It will download all the required things and will make build.
* No Database required
* Now after build successful, navigate in the target folder ,you will see football-penalty-1.0.jar

To run the jar use following command from your terminal.

java -cp football-penalty-1.0.jar com.penalties.football.FootballPenaltyApplication

Please make sure that you change serialization path in the file application.properties in resources folder before starting build process.

FootballPenaltyApplication.java is the starting point of this application. Main method reside in this file.
