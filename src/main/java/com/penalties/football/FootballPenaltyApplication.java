package com.penalties.football;

import com.penalties.football.app.config.FootballPenaltiesConfig;

public class FootballPenaltyApplication {

  public static void main(String[] args) {

    FootballPenaltiesConfig.begin();
  }

}
