package com.penalties.football.app.game;

import static java.util.Objects.requireNonNull;

import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.PlayerLevel;
import com.penalties.football.app.constant.CommonEnum.PlayerType;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.AbstractPresenter;

public class PlayerPresenter extends AbstractPresenter<PlayerView> implements PlayerView.ActionDelegate, PlayerFactory {

	private String name;

	private StrongerFoot strongerFoot;

	private LeagueTitle leagueTitle;

	public PlayerPresenter(PlayerView view) {
		super(view);
		view.setDelegate(this);
	}

	@Override
	public void onChoosen(String name) {
		this.name = name;

	}

	@Override
	public void onChoosen(StrongerFoot strongerFoot) {
		this.strongerFoot = strongerFoot;

	}

	@Override
	public void onChoosen(LeagueTitle leagueTitle) {
		this.leagueTitle = leagueTitle;
	}

	@Override
	public void onCompleted() {
		requireNonNull(name, "You must choose a name for the striker.");
		requireNonNull(strongerFoot, "Striker would love it if you define his stronger foot.");
		requireNonNull(leagueTitle, "Hey Striker wants to know which League he is playing in.");
	}

	public String getName() {
		return name;
	}

	public StrongerFoot getStrongerFoot() {
		return strongerFoot;
	}

	public LeagueTitle getLeagueTitle() {
		return leagueTitle;
	}

	@Override
	public Player getPlayer() {
		show();
		return new Player(name, PlayerLevel.Beginner, PlayerType.STRIKER, leagueTitle, strongerFoot);
	}
}
