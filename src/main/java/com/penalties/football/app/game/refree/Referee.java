package com.penalties.football.app.game.refree;

import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.model.Player;

public interface Referee {

	void start();

	Player getStriker();

	Player getGoalKeeper();

	Game getGame();

	Game loadGame();

	GameStatus getGameStatus();

	void declareResult();

	void setStrikerExperience(Player striker);
}
