package com.penalties.football.app.game;

import java.util.Optional;

import com.penalties.football.app.model.Player;

public interface GameOrganizerFactory {

	
	Optional<GameOrganizer> create();

    Optional<GameOrganizer> load();

    Optional<GameOrganizer> organizeGameForProfile();

    Optional<Player> getStriker();

}
