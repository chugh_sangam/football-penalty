package com.penalties.football.app.game.refree;

import java.io.IOException;
import java.time.LocalDateTime;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.constant.CommonEnum.PlayerLevel;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.serializers.FootballPenaltyObjectDeSerializer;

public class RefereeInCharge implements Referee {

	private final GameStrategy gameStrategy;

	private final Player striker;

	private final Player goalKeeper;

	private Game game;

	public RefereeInCharge(final Player striker, final Player goalKeeper, final GameStrategy gameStrategy) {
		this.gameStrategy = gameStrategy;
		this.striker = striker;
		this.goalKeeper = goalKeeper;
	}

	@Override
	public void start() {

		if (this.game == null)
			this.game = new Game(LocalDateTime.now(), this.striker.getLeagueTitle(), CommonConstant.SHOT_START_COUNTER);
		gameStrategy.start(this.striker, this.goalKeeper, this.game);
		this.striker.getExperience().getGames().add(game);
		setStrikerExperience(this.striker);
		this.goalKeeper.getExperience().getGames().add(game);

	}

	@Override
	public Player getStriker() {
		return this.striker;
	}

	@Override
	public Player getGoalKeeper() {
		return this.goalKeeper;
	}

	@Override
	public GameStatus getGameStatus() {
		return this.gameStrategy.getGameStatus();
	}

	@Override
	public void declareResult() {
		this.game.setResult(this.striker, this.goalKeeper);
	}

	@Override
	public Game getGame() {
		return this.game;
	}

	@Override
	public Game loadGame() {
		try {
			this.game = (Game) FootballPenaltyObjectDeSerializer.deSerialize(CommonConstant.CURRENT_GAME);
		} catch (ClassNotFoundException | IOException e) {
			System.err.println("Unable to load the previously saved Game.");
			System.err.println(e.getMessage());
		}
		return null;
	}

	@Override
	public void setStrikerExperience(Player striker) {
		int gamesCount = striker.getExperience().getGames().size();
		switch (gamesCount) {
		case 2:
			striker.setPlayerLevel(PlayerLevel.Amatuer);
			break;
		case 4:
			striker.setPlayerLevel(PlayerLevel.Regular);
			break;
		case 6:
			striker.setPlayerLevel(PlayerLevel.Professional);
			break;
		case 8:
			striker.setPlayerLevel(PlayerLevel.Superstar);
			break;
		default:
			break;
		}
	}

}
