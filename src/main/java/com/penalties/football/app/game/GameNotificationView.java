package com.penalties.football.app.game;

import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.ui.component.AbstractConsoleView;
import com.penalties.football.app.ui.component.NotificationView;

public class GameNotificationView extends AbstractConsoleView<NotificationView.ActionDelegate>
		implements NotificationView {

	@Override
	public void draw() {
		throw new UnsupportedOperationException("This method is not useful");
	}

	@Override
	public void showWinnerNotification(Referee referee) {
		referee.declareResult();
		Game game = referee.getGame();

		if (game.getGoalDefended() > game.getGoalScored()) {
			System.out.println("The Goalkeeper Mr. " + referee.getGoalKeeper().getName()
					+ " has won this penalty shootout with score " + game.getGoalDefended() + "-"
					+ game.getGoalScored());
			game.setRunnerup(referee.getStriker().getName());
		} else if (game.getGoalDefended() < game.getGoalScored()) {
			System.out.println(
					"The Striker Mr. " + referee.getStriker().getName() + " has won this penalty shootout with score "
							+ game.getGoalScored() + "-" + game.getGoalDefended());
		}

	}

	@Override
	public void showPausedNotification() {
		System.out.println("You have selected to pause this game. We will be waiting for you to be back soon..");

	}

}
