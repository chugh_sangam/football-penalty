package com.penalties.football.app.game;

import static java.lang.String.format;

import com.penalties.football.app.constant.CommonEnum.PlayerType;
import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.AbstractConsoleView;
import com.penalties.football.app.ui.component.GameView;
import com.penalties.football.app.ui.component.Menu;

public class GameActionView extends AbstractConsoleView<GameView.ActionDelegate> implements GameView {

	private Menu<ShotType> shotType;

	public GameActionView(final Menu<ShotType> shotType) {
		this.shotType = shotType;
	}

	@Override
	public void draw() {
		shotType.draw();
		ShotType selectedShot = shotType.selectItem();
		switch (selectedShot) {
		case SEG:
			delegate.onSaveAndExit();
			break;
		default:
			delegate.onShotSelection(selectedShot);
			break;
		}

	}

	@Override
	public void drawStriker(Player player) {
		drawPlayer(player);

	}

	@Override
	public void drawGoalKeeper(Player player) {
		drawPlayer(player);
	}

	@Override
	public void drawResultOnShotSelection(Player player) {
		if (player.getPlayerType() == PlayerType.GOALKEEPER)
			System.out.println("\n" + player.getName() + " has saved the goal beautifully");
		else if (player.getPlayerType() == PlayerType.STRIKER)
			System.out.println("\nUuh.. That was an amazing strike and " + player.getName() + " has scored a goal");

	}

	/**
	 * @param player
	 *            Print Player attributes
	 */
	private void drawPlayer(final Player player) {
		System.out.println(format("Name: %s;  PlayerType: %s;  Player Level: %s;  League Title: %s; Experience: %s",
				player.getName(), player.getPlayerType(), player.getPlayerLevel(), player.getLeagueTitle(),
				player.getExperience().getGames()));

	}

}
