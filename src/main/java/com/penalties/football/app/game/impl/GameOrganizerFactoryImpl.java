package com.penalties.football.app.game.impl;

import java.util.Optional;

import com.penalties.football.app.game.GameInitializer;
import com.penalties.football.app.game.GameOrganizer;
import com.penalties.football.app.game.GameOrganizerFactory;
import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.NotificationView;

public class GameOrganizerFactoryImpl implements GameOrganizerFactory {

	final private GameInitializer gameIntializer;
	final private NotificationView view;

	public GameOrganizerFactoryImpl(final GameInitializer gameInitializer, final NotificationView view) {
		this.gameIntializer = gameInitializer;
		this.view = view;
	}

	@Override
	public Optional<GameOrganizer> create() {
		return create(gameIntializer.create());
	}

	private Optional<GameOrganizer> create(final Optional<Referee> optional) {
		if (optional.isPresent()) {
			return Optional.of(new GameOrganizerImpl(optional.get(), view));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<GameOrganizer> load() {
		return create(gameIntializer.load());
	}

	@Override
	public Optional<GameOrganizer> organizeGameForProfile() {
		return create(gameIntializer.findNewGoalKeeper());
	}

	@Override
	public Optional<Player> getStriker() {
		return this.gameIntializer.getStriker();

	}

}
