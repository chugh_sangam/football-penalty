package com.penalties.football.app.game;

import com.penalties.football.app.model.Player;

public interface PlayerFactory {

	Player getPlayer();

}
