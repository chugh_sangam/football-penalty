package com.penalties.football.app.game;

import java.util.Optional;

import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.model.Player;

public interface GameInitializer {
	
	  /**
     * @return create referee instance for the first game.
     */
    Optional<Referee> create();

    /**
     * @return load saved game from file then create and return Referee instance  
     */
    Optional<Referee> load();

    /**
     * @return create referee instance when user choose to play next game of penalties 
     * with same profile and return that instance. 
     * It uses choose random goalkeeper from existing list of Goalkeepers.  
     */
    Optional<Referee> findNewGoalKeeper();

    /**
     * @return striker
     */
    Optional<Player> getStriker();

}
