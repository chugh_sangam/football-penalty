package com.penalties.football.app.game.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.GoalKeeper;
import com.penalties.football.app.constant.CommonEnum.PlayerLevel;
import com.penalties.football.app.constant.CommonEnum.PlayerType;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.game.GameInitializer;
import com.penalties.football.app.game.PlayerFactory;
import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.game.refree.RefereeInCharge;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.serializers.FootballPenaltyObjectDeSerializer;
import com.penalties.football.app.utils.RandomIntInRangeUtil;
import com.penalties.football.app.utils.RandomSelectedEnumUtil;

public class GameIntializerImpl implements GameInitializer {

	private Player striker;

	private Player goalKeeper;

	private List<Player> goalKeepers;

	private final PlayerFactory playerFactory;

	private final GameStrategy gameStrategy;

	public GameIntializerImpl(final PlayerFactory playerFactory, final GameStrategy gameStrategy) {
		this.playerFactory = playerFactory;
		this.gameStrategy = gameStrategy;
	}

	@Override
	public Optional<Referee> create() {
		this.striker = this.playerFactory.getPlayer();
		this.goalKeepers = createGoalKeepers();
		this.goalKeeper = this.goalKeepers.get(RandomIntInRangeUtil.get(0, goalKeepers.size()));
		this.goalKeeper.setLeagueTitle(this.striker.getLeagueTitle());
		return Optional.of(new RefereeInCharge(this.striker, this.goalKeeper, gameStrategy));
	}

	@Override
	public Optional<Referee> load() {
		try {
			this.striker = (Player) FootballPenaltyObjectDeSerializer.deSerialize(CommonConstant.STRIKER);
			this.goalKeeper = (Player) FootballPenaltyObjectDeSerializer.deSerialize(CommonConstant.GOAL_KEEPER);
			this.goalKeeper.setLeagueTitle(this.striker.getLeagueTitle());
			this.goalKeepers = createGoalKeepers();
			Referee referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
			referee.loadGame();
			return Optional.of(referee);
		} catch (ClassNotFoundException | IOException e) {
			System.err.println(e.getMessage());
		}

		return Optional.empty();
	}

	@Override
	public Optional<Referee> findNewGoalKeeper() {
		if (this.goalKeepers != null) {
			this.goalKeeper = this.goalKeepers.get(RandomIntInRangeUtil.get(0, this.goalKeepers.size()));
			this.goalKeeper.setLeagueTitle(this.striker.getLeagueTitle());
			return Optional.of(new RefereeInCharge(this.striker, this.goalKeeper, gameStrategy));
		}
		return Optional.empty();
	}

	@Override
	public Optional<Player> getStriker() {
		if (this.striker != null)
			return Optional.of(striker);
		return Optional.empty();
	}

	private List<Player> createGoalKeepers() {
		List<Player> goalKeepers = new ArrayList<>();
		Player goalKeeper = null;
		StrongerFoot strongerFoot;
		PlayerLevel playerLevel;
		for (final GoalKeeper keeper : GoalKeeper.values()) {
			strongerFoot = RandomSelectedEnumUtil.menu(EnumSet.allOf(StrongerFoot.class));
			playerLevel = RandomSelectedEnumUtil.menu(EnumSet.allOf(PlayerLevel.class));
			goalKeeper = new Player(keeper.toString(), playerLevel, PlayerType.GOALKEEPER, null, strongerFoot);
			goalKeepers.add(goalKeeper);
		}

		return goalKeepers;
	}
}
