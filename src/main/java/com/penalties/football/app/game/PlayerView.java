package com.penalties.football.app.game;

import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.ui.component.CommandLineView;

public interface PlayerView extends CommandLineView<PlayerView.ActionDelegate> {

	interface ActionDelegate {
		void onChoosen(String name);

		void onChoosen(StrongerFoot strongerFoot);

		void onChoosen(LeagueTitle leagueTitle);

		void onCompleted();
	}
}
