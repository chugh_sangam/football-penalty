package com.penalties.football.app.game.strategy;

import java.io.IOException;
import java.util.EnumSet;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.serializers.FootballPenaltyObjectSerializer;
import com.penalties.football.app.ui.component.AbstractPresenter;
import com.penalties.football.app.ui.component.GameView;
import com.penalties.football.app.utils.RandomSelectedEnumUtil;

public class InitialGameStrategy extends AbstractPresenter<GameView> implements GameStrategy, GameView.ActionDelegate {

	public InitialGameStrategy(GameView view) {
		super(view);
		this.view.setDelegate(this);
	}

	private Player striker;
	private Player goalKeeper;

	private Game game;
	private GameStatus gameStatus;

	@Override
	public void start(Player striker, Player goalKeeper, Game game) {
		this.striker = striker;
		this.goalKeeper = goalKeeper;
		this.game = game;
		startGame();
	}

	private void startGame() {
		this.gameStatus = GameStatus.IN_PROGRESS;
		nextIteration();
		if (this.gameStatus != GameStatus.SAVED) {
			this.gameStatus = GameStatus.COMPLETED;
		}

	}

	private void nextIteration() {
		if (this.game.canPlay()) {
			view.drawStriker(this.striker);
			view.drawGoalKeeper(this.goalKeeper);
			show();
		}
	}

	@Override
	public GameStatus getGameStatus() {
		return this.gameStatus;
	}

	public ShotType getGoalKeeperReaction() {
		return RandomSelectedEnumUtil.menu(EnumSet.allOf(ShotType.class));
	}

	@Override
	public void onSaveAndExit() {
		try {
			FootballPenaltyObjectSerializer.serialize(CommonConstant.STRIKER, striker);
			FootballPenaltyObjectSerializer.serialize(CommonConstant.GOAL_KEEPER, goalKeeper);
			FootballPenaltyObjectSerializer.serialize(CommonConstant.CURRENT_GAME, game);
			this.gameStatus = GameStatus.SAVED;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onShotSelection(ShotType shotType) {
		ShotType goalKeeperShotSelection = getGoalKeeperReaction();
		if (shotType == goalKeeperShotSelection) {
			this.game.setGoalDefended(this.game.getGoalDefended() + 1);
			view.drawResultOnShotSelection(goalKeeper);
		} else if (shotType != goalKeeperShotSelection) {
			this.game.setGoalScored(this.game.getGoalScored() + 1);
			view.drawResultOnShotSelection(striker);
		}
		this.game.setShotNumber(this.game.getShotNumber() + 1);
		nextIteration();
	}

}
