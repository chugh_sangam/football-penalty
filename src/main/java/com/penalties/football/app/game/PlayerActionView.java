package com.penalties.football.app.game;

import java.util.logging.Logger;

import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.ui.component.AbstractConsoleView;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.ui.component.TextInput;

public class PlayerActionView extends AbstractConsoleView<PlayerView.ActionDelegate> implements PlayerView {

	private final TextInput input;

	private final Menu<StrongerFoot> strongerFoot;

	private final Menu<LeagueTitle> leagueTitle;

	Logger logger = Logger.getLogger(this.getClass().getName());

	public PlayerActionView(final TextInput input, final Menu<StrongerFoot> strongerFoot,
			final Menu<LeagueTitle> leagueTitle) {
		this.input = input;
		this.strongerFoot = strongerFoot;
		this.leagueTitle = leagueTitle;
	}

	@Override
	public void draw() {
		System.out.println("\nNew Player Menu");

		input.draw();
		delegate.onChoosen(input.getValue());

		strongerFoot.draw();
		delegate.onChoosen(strongerFoot.selectItem());

		leagueTitle.draw();
		delegate.onChoosen(leagueTitle.selectItem());

		delegate.onCompleted();

	}

}
