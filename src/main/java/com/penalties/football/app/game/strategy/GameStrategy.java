package com.penalties.football.app.game.strategy;

import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.model.Player;

public interface GameStrategy {

	/**
	 * @param striker
	 * @param goalKeeper
	 * @param game
	 */
	void start(Player striker, Player goalKeeper, Game game);

	/**
	 * @return Return game current status
	 */
	GameStatus getGameStatus();
}