package com.penalties.football.app.game.impl;

import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.game.GameOrganizer;
import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.ui.component.NotificationView;

public class GameOrganizerImpl implements GameOrganizer {

	private Referee referee;

	private NotificationView notifiactionView;

	public GameOrganizerImpl(Referee referee, NotificationView notificationView) {
		this.notifiactionView = notificationView;
		this.referee = referee;
	}

	@Override
	public void start() {
		referee.start();
		showGameStatusNotification();
	}

	private void showGameStatusNotification() {
		GameStatus gameStatus = this.referee.getGameStatus();
		switch (gameStatus) {
		case SAVED:
			notifiactionView.showPausedNotification();
			break;
		case COMPLETED:
			notifiactionView.showWinnerNotification(this.referee);
			break;
		default:
			break;
		}
	}
}
