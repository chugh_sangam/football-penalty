package com.penalties.football.app.constant;

public class CommonEnum {

	public enum GoalKeeper {
		ALISSON_BECKER("Alisson Becker"), MANUEL_NEUER("Manuel Neuer"), JAN_OBLAK("Jan Oblak"), JORDAN_PICKFORD(
				"Jordan Pickford"), IKER_CASILLAS("Iker Casillas"), HUGO_LLORIS(
						"Hugo Lloris"), THIBAUT_COURTOIS("Thiabaut Courtois"), OLIVER_KAHN("Oliver Kahn");
		private final String value;

		private GoalKeeper(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum PlayerLevel {
		Beginner, Amatuer, Regular, Professional, Superstar;

	}

	public enum LeagueTitle {
		CL("Champions League"), PL("Premiur League"), LL("Laliga"), CDR("Copa Del Rey"), LO("Ligue 1"), FL(
				"French League"), BL("Bundesliga");

		private String leagueName;

		private LeagueTitle(String leagueName) {
			this.leagueName = leagueName;
		}

		@Override
		public String toString() {
			return leagueName;
		}
	}

	public enum GameStatus {
		IN_PROGRESS("In progress"), SAVED("Paused and saved."), COMPLETED("Completed");

		private final String value;

		private GameStatus(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum PlayerType {
		STRIKER, GOALKEEPER;
	}

	public enum StrongerFoot {
		RIGHTFOOT("Right Foot"), LEFTFOOT("Left Foot");

		private String value;

		private StrongerFoot(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}

	}

	public enum ShotType {
		LLC("Left Lower Corner"), RLC("Right Lower Corner"), LUC("Left Upper Corner"), RUC("Right Upper Corner"), CS(
				"Center Shot"), SEG("Save and Exit Game");

		private String value;

		private ShotType(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

}
