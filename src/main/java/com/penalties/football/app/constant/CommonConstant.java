package com.penalties.football.app.constant;

/**
 * @author sangamchugh
 */
public final class CommonConstant {
	public static final int MENU_ITEM_OFFSET = 1;
	public static final int SHOT_START_COUNTER = 1;
	public static final int MAX_SHOT_AVAILABLE = 5;
	public static final String GOAL_KEEPER = "goalKeeper.ser";
	public static final String STRIKER = "striker.ser";
	public static final String CURRENT_GAME = "currentGame.ser";
	public static final String PROP_SERIALIZATION_FILE_PATH = "serialization.path";

	private CommonConstant() {

	}
}
