package com.penalties.football.app.utils;

import static java.util.Objects.nonNull;

import java.util.Scanner;
import java.util.function.Predicate;

public enum UserInputReader {

	INSTANCE;

	private final Scanner sc;

	private UserInputReader() {
		sc = new Scanner(System.in, "UTF-8");
	}

	public String readString() {
		return sc.nextLine();
	}

	public int readIntegerUntilValid(Predicate<String> userCondition, Runnable onFail) {
		final Predicate<String> retryCondition = LambdaUtils.VALID_INPUT_DATA_CONDITION.and(userCondition).negate();
		String line = null;
		do {
			if (nonNull(line)) {
				onFail.run();
			}
			line = readString().trim();
		} while (retryCondition.test(line));
		return Integer.parseInt(line);
	}

}
