package com.penalties.football.app.serializers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.penalties.football.app.config.PropertyLoader;

public class FootballPenaltyObjectDeSerializer {
	
	 private static final String PATH =  PropertyLoader.INSTANCE.getPropertyValue("serialization.path"); ;
	 
    public static Object deSerialize(final String fileName) throws IOException, ClassNotFoundException {
        try(FileInputStream file = new FileInputStream(PATH+fileName);ObjectInputStream in = new ObjectInputStream
                (file);) {
           return in.readObject();
        }
    }

}
