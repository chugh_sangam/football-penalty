package com.penalties.football.app.serializers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.penalties.football.app.config.PropertyLoader;



public class FootballPenaltyObjectSerializer {
	
	 private static final String PATH =  PropertyLoader.INSTANCE.getPropertyValue("serialization.path"); ;
	    private FootballPenaltyObjectSerializer() {

	    }

	    public static void serialize(final String fileName,final Object object) throws IOException {
	       final File file = new File(PATH+fileName);
	        if(!file.exists()) {
	            file.createNewFile();
	        }
	        try(FileOutputStream fileOS = new FileOutputStream(file);
	                ObjectOutputStream out = new ObjectOutputStream(fileOS);){
	            out.writeObject(object);

	        }
	    }

}
