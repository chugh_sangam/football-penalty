package com.penalties.football.app.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum PropertyLoader {
	INSTANCE;
	private Properties props = new Properties();

	private PropertyLoader() {
		InputStream inputStream;
		final String propFileName = "application.properties";
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		if (inputStream != null) {
			try {
				props.load(inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getPropertyValue(final String property) {
		if (props.containsKey(property)) {
			return props.getProperty(property);
		}
		return null;
	}

}
