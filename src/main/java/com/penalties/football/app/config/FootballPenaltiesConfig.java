package com.penalties.football.app.config;

import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.game.GameActionView;
import com.penalties.football.app.game.GameInitializer;
import com.penalties.football.app.game.GameNotificationView;
import com.penalties.football.app.game.GameOrganizerFactory;
import com.penalties.football.app.game.PlayerActionView;
import com.penalties.football.app.game.PlayerPresenter;
import com.penalties.football.app.game.impl.GameIntializerImpl;
import com.penalties.football.app.game.impl.GameOrganizerFactoryImpl;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.game.strategy.InitialGameStrategy;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.ui.component.NotificationView;
import com.penalties.football.app.ui.component.TextInput;
import com.penalties.football.app.ui.view.WelcomeMenuConsoleView;
import com.penalties.football.app.ui.view.WelcomeMenuConsoleView.MainMenuItem;
import com.penalties.football.app.ui.view.WelcomeMenuPresenter;

public class FootballPenaltiesConfig {

	public static void begin() {
		final Menu<MainMenuItem> mainMenu = new Menu<>("Welcome to Football Penalty Shoutout Game!!",
				MainMenuItem.values());
		new WelcomeMenuPresenter(new WelcomeMenuConsoleView(mainMenu), gameOrganizerFactory()).show();

	}

	private static GameOrganizerFactory gameOrganizerFactory() {
		return new GameOrganizerFactoryImpl(gameInitializer(), gameNotificationView());
	}

	private static GameInitializer gameInitializer() {
		return new GameIntializerImpl(playerPresenter(), gameStrategy());
	}

	private static NotificationView gameNotificationView() {
		return new GameNotificationView();
	}

	private static GameStrategy gameStrategy() {
		final Menu<ShotType> shotSelections = new Menu<>("Please do the shot selection : ", ShotType.values());
		return new InitialGameStrategy(new GameActionView(shotSelections));
	}

	private static PlayerPresenter playerPresenter() {
		final TextInput playerName = new TextInput("Lets name your Striker:");
		Menu<StrongerFoot> strongerfoot = new Menu<>("Select the stronger foot of striker :", StrongerFoot.values());
		Menu<LeagueTitle> leagueTitle = new Menu<>("Select the League of the game :", LeagueTitle.values());
		return new PlayerPresenter(new PlayerActionView(playerName, strongerfoot, leagueTitle));
	}
}
