package com.penalties.football.app.ui.component;

import com.penalties.football.app.game.refree.Referee;

public interface NotificationView extends CommandLineView<NotificationView.ActionDelegate> {

	void showWinnerNotification(Referee referee);

	void showPausedNotification();

	interface ActionDelegate {

	}
}
