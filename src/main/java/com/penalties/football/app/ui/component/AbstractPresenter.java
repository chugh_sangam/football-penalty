package com.penalties.football.app.ui.component;


@SuppressWarnings("rawtypes")
public abstract class AbstractPresenter<T extends CommandLineView> implements Presenter {

    protected final T view;

    public AbstractPresenter(final T view) {
        this.view = view;
    }

    @Override
    public void show() {
        view.draw();
    }
}

