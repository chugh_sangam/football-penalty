package com.penalties.football.app.ui.component;

public abstract class AbstractConsoleView<T> implements Component, CommandLineView<T> {

	protected T delegate;

	@Override
	public void setDelegate(T delegate) {
		this.delegate = delegate;

	}

	@Override
	public void erase() {
		Component.super.erase();

	}

}
