package com.penalties.football.app.ui.view;

import com.penalties.football.app.ui.component.AbstractConsoleView;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.ui.view.WelcomeMenuView.ActionDelegate;

public class WelcomeMenuConsoleView extends AbstractConsoleView<ActionDelegate> implements WelcomeMenuView {

	private Menu<MainMenuItem> menu;

	public WelcomeMenuConsoleView(final Menu<MainMenuItem> menu) {
		super();
		this.menu = menu;
	}

	public enum MainMenuItem {

		START_NEW("Lets Start game with a new Profile"), START_NEXT(
				"Lets start a new game with same Striker"), START_SAVED_GAME(
						"Please load existing profile."), VIEW_PLAYER_PROFILE(
								"View players current profile."), EXIT("Exit the game");

		private final String title;

		MainMenuItem(String title) {
			this.title = title;

		}

		@Override
		public String toString() {
			return title;
		}
	}

	@Override
	public void draw() {

		menu.draw();

		switch (menu.selectItem()) {
		case START_NEW:
			delegate.onStartChosen();
			break;
		case START_NEXT:
			delegate.onContiWithExistingProfileChosen();
			break;
		case START_SAVED_GAME:
			System.out.println("\nLets load the previously saved game and players profile.");
			delegate.onResumeChosen();
			break;
		case VIEW_PLAYER_PROFILE:
			delegate.onviewProfileChosen();
			break;
		case EXIT:
			delegate.onExitChosen();
			break;
		}

	}

}
