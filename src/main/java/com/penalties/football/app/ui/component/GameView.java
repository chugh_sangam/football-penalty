package com.penalties.football.app.ui.component;

import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.model.Player;

public interface GameView extends CommandLineView<GameView.ActionDelegate> {

	void drawStriker(Player player);

	void drawGoalKeeper(Player player);

	void drawResultOnShotSelection(Player player);

	interface ActionDelegate {

		void onShotSelection(ShotType shotType);

		void onSaveAndExit();
	}

}
