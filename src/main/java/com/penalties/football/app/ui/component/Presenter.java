package com.penalties.football.app.ui.component;

public interface Presenter {

	void show();
}
