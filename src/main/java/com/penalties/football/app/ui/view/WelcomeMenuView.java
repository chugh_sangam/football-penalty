package com.penalties.football.app.ui.view;

import com.penalties.football.app.ui.component.CommandLineView;

public interface WelcomeMenuView extends CommandLineView<WelcomeMenuView.ActionDelegate> {
	interface ActionDelegate {

		/**
		 * When user choose to create new game with new profile.
		 */
		void onStartChosen();

		/**
		 * When user choose to play new game with same profile
		 */
		void onContiWithExistingProfileChosen();

		/**
		 * When user choose to resume the game that was saved
		 */
		void onResumeChosen();

		/**
		 * When user choose to see profile
		 */
		void onviewProfileChosen();

		void onExitChosen();
	}

}