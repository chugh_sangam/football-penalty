package com.penalties.football.app.ui.component;

public interface CommandLineView<T> {


/**
 * @param delegate
 * Set delegate to which control return after user choose from options menu
 */
void setDelegate(T delegate);

/**
 * draw
 */
void draw();

/**
 * erase
 */
void erase();

}