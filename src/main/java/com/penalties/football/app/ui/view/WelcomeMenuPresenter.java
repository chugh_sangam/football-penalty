package com.penalties.football.app.ui.view;

import java.util.Optional;

import com.penalties.football.app.game.GameOrganizer;
import com.penalties.football.app.game.GameOrganizerFactory;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.AbstractPresenter;

public class WelcomeMenuPresenter extends AbstractPresenter<WelcomeMenuView> implements WelcomeMenuView.ActionDelegate {

	private GameOrganizerFactory gameOrganizerFactory;

	public WelcomeMenuPresenter(WelcomeMenuView view, GameOrganizerFactory gameOrganizerFactory) {
		super(view);
		this.view.setDelegate(this);
		this.gameOrganizerFactory = gameOrganizerFactory;
	}

	@Override
	public void onStartChosen() {
		Optional<GameOrganizer> optional = this.gameOrganizerFactory.create();
		if (optional.isPresent()) {
			optional.get().start();
		} else {
			System.out.println("Sorry could not create game.");
		}
		show();
	}

	@Override
	public void onContiWithExistingProfileChosen() {

		final Optional<GameOrganizer> optional = gameOrganizerFactory.organizeGameForProfile();
		if (optional.isPresent()) {
			optional.get().start();
		} else {
			System.out.println("\n Sorry, We cannot find any profile for now.Please, try making a new Stiker.");
		}
		show();
	}

	@Override
	public void onResumeChosen() {
		final Optional<GameOrganizer> optional = gameOrganizerFactory.load();
		if (optional.isPresent()) {
			optional.get().start();
		} else {
			System.out.println("Sorry could not load the previous game at this time.");
		}
		show();

	}

	@Override
	public void onviewProfileChosen() {
		final Optional<Player> optional = gameOrganizerFactory.getStriker();
		if (optional.isPresent()) {
			System.out.println(optional.get());
		} else {
			System.out.println("Profile does not exist. Please, try making a new Stiker.");
		}
		show();
	}

	@Override
	public void onExitChosen() {
		System.out.println("Thanks for playing the Football Penalty. Hope you had a great time. See you soon..");

	}

}
