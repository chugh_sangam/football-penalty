package com.penalties.football.app.ui.component;

import java.util.function.Predicate;
import java.util.stream.Stream;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.utils.IntRange;
import com.penalties.football.app.utils.LambdaUtils;
import com.penalties.football.app.utils.UserInputReader;

@SuppressWarnings("rawtypes")
public class Menu<T extends Enum> implements Component {

	/**
	 * Menu title
	 */
	private final String title;

	/**
	 * Menu Items
	 */
	private final T[] items;

	/**
	 * represent acceptable option selection range. eg if there are 3 menu item in
	 * menu then its range is 1 to 3
	 */
	private final IntRange acceptableItems;

	/**
	 * Draw warning message about invalid user option selection
	 */
	private final Runnable redrawWithWarningMessage = () -> {
		redraw();
		printMenuFooter(true);
	};

	private void printMenuFooter(final boolean hasToPrintWarning) {
		if (hasToPrintWarning) {
			System.out.println("Invalid Action. Please, type again.");
		}
		System.out.println("Select action number:");
	}

	@SafeVarargs
	public Menu(final String title, final T... items) {

		if (items.length == 0) {
			throw new IllegalArgumentException("Should have atleast one Menu item.");
		}
		this.title = title;
		this.items = items;
		this.acceptableItems = IntRange.of(1, items.length);
	}

	@Override
	public void draw() {
		System.out.println("\n" + title);
		Stream.of(items).map(LambdaUtils.ENUM_TO_STRING).forEach(System.out::println);
	}

	/**
	 * @return
	 * 
	 * 		Select item from menu items
	 */
	public T selectItem() {
		printMenuFooter(false);
		return items[readItemIndex()];
	}

	private int readItemIndex() {
		return UserInputReader.INSTANCE.readIntegerUntilValid(itemIsInAcceptableRange(), redrawWithWarningMessage)
				- CommonConstant.MENU_ITEM_OFFSET;
	}

	public T[] getItems() {
		return items;
	}

	public IntRange getAcceptableItems() {
		return acceptableItems;
	}

	private Predicate<String> itemIsInAcceptableRange() {
		return line -> acceptableItems.contains(Integer.parseInt(line));
	}

}
