package com.penalties.football.app.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.utils.LambdaUtils;

public class Game implements Serializable {

	private static final long serialVersionUID = 6041448306632640340L;

	private LocalDateTime gameTime;

	private String winner;

	private String runnerup;

	private LeagueTitle leagueTitle;

	private int goalScored;

	private int goalDefended;

	private int shotNumber;

	public Game(final LocalDateTime gameTime, final LeagueTitle leagueTitle, int shotNumber) {
		this.gameTime = gameTime;
		this.leagueTitle = leagueTitle;
		this.shotNumber = shotNumber;
	}

	public LocalDateTime getGameTime() {
		return gameTime;
	}

	public void setGameTime(LocalDateTime gameTime) {
		this.gameTime = gameTime;
	}

	public int getGoalDefended() {
		return goalDefended;
	}

	public void setGoalDefended(int goalDefended) {
		this.goalDefended = goalDefended;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public String getRunnerup() {
		return runnerup;
	}

	public void setRunnerup(String runnerup) {
		this.runnerup = runnerup;
	}

	public LeagueTitle getLeagueTitle() {
		return leagueTitle;
	}

	public void setLeagueTitle(LeagueTitle leagueTitle) {
		this.leagueTitle = leagueTitle;
	}

	public int getGoalScored() {
		return goalScored;
	}

	public void setGoalScored(int goalScored) {
		this.goalScored = goalScored;
	}

	public int getShotNumber() {
		return shotNumber;
	}

	public void setShotNumber(int shotNumber) {
		this.shotNumber = shotNumber;
	}

	public boolean canPlay() {
		if (LambdaUtils.VALID_INPUT_DATA_CONDITION.test(String.valueOf(this.shotNumber))
				&& (this.shotNumber <= CommonConstant.MAX_SHOT_AVAILABLE))
			return true;
		return false;
	}

	public void setResult(final Player striker, final Player goalKeeper) {
		if (this.getGoalDefended() > this.getGoalScored()) {
			winner = goalKeeper.getName();
			runnerup = striker.getName();
		} else if (this.getGoalDefended() < this.getGoalScored()) {
			winner = striker.getName();
			runnerup = goalKeeper.getName();
		}
	}

	@Override
	public String toString() {
		return "Game =[gameTime=" + gameTime + ", winner=" + winner + ", runnerUp =" + runnerup + ", leagueTitle="
				+ leagueTitle + ", goalScored=" + goalScored + ", goalDefended=" + goalDefended + " ]";
	}

}
