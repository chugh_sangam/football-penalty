package com.penalties.football.app.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author sangamchugh
 *
 */
public class Experience implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6861674022600011492L;

	private List<Game> games;

	public Experience() {
		this.games = new ArrayList<Game>();
	}

	public List<Game> getGames() {
		return games;
	}

	@Override
	public String toString() {
		return "Experience= [games = " + games + "]";
	}
}
