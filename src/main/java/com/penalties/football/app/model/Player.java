package com.penalties.football.app.model;

import java.io.Serializable;

import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.PlayerLevel;
import com.penalties.football.app.constant.CommonEnum.PlayerType;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;

/**
 * 
 * @author sangamchugh
 * 
 */

public class Player implements Serializable {

	private static final long serialVersionUID = 2340541721890285318L;

	private String name;

	private Experience experience;

	private PlayerLevel playerLevel;

	private PlayerType playerType;

	private LeagueTitle leagueTitle;

	private StrongerFoot strongerFoot;

	public Player() {
	}

	public Player(final String name, final PlayerLevel playerLevel, final PlayerType playerType,
			final LeagueTitle leagueTitle, final StrongerFoot strongerFoot) {
		this.name = name;
		this.playerLevel = playerLevel;
		this.strongerFoot = strongerFoot;
		this.playerType = playerType;
		this.leagueTitle = leagueTitle;
		this.experience = new Experience();
	}

	public String getName() {
		return name;
	}

	public Experience getExperience() {
		return experience;
	}

	public PlayerType getPlayerType() {
		return playerType;
	}

	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}

	public StrongerFoot getStrongerFoot() {
		return strongerFoot;
	}

	public void setStrongerFoot(StrongerFoot strongerFoot) {
		this.strongerFoot = strongerFoot;
	}

	public LeagueTitle getLeagueTitle() {
		return leagueTitle;
	}

	public void setLeagueTitle(LeagueTitle leagueTitle) {
		this.leagueTitle = leagueTitle;
	}

	public PlayerLevel getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(PlayerLevel playerLevel) {
		this.playerLevel = playerLevel;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", playerLevel=" + playerLevel + ", playerType=" + playerType + ""
				+ ", leagueTitle=" + leagueTitle + ", " + "strongerFoot=" + strongerFoot + ", experience=" + experience
				+ "]";
	}

}
