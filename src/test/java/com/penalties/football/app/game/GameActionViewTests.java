package com.penalties.football.app.game;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.utils.PlayerBuilderHelper;
import com.penalties.football.app.utils.PrintStreamHelper;

public class GameActionViewTests extends PrintStreamHelper {

	Menu<ShotType> menu;

	GameActionView gameActionView;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {
		menu = mock(Menu.class);
		gameActionView = new GameActionView(menu);
	}

	@Test
	public void testDrawStriker() {
		Player striker = PlayerBuilderHelper.createStriker();
		gameActionView.drawStriker(striker);
		assertThat(outContent.toString(), containsString(striker.getName()));
	}

	@Test
	public void testDrawGoalKepeer() {
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		gameActionView.drawGoalKeeper(goalKeeper);
		assertThat(outContent.toString(), containsString(goalKeeper.getName()));
	}

	@Test
	public void testDrawResultOnShotSelection() {
		Player player = PlayerBuilderHelper.createGoalKeeper();
		gameActionView.drawResultOnShotSelection(player);
		assertThat(outContent.toString(), containsString("saved the goal"));

		player = PlayerBuilderHelper.createStriker();
		gameActionView.drawResultOnShotSelection(player);
		assertThat(outContent.toString(), containsString("an amazing strike"));
	}

}
