package com.penalties.football.app.game;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.ui.component.TextInput;
import com.penalties.football.app.utils.PrintStreamHelper;

public class PlayerPresenterTests extends PrintStreamHelper {

	@SuppressWarnings("unchecked")
	@Test
	public void testOnCompleteShouldCreatePlayer() {
		TextInput playerName = mock(TextInput.class);

		Menu<LeagueTitle> leagueTitle = mock(Menu.class);
		Menu<StrongerFoot> strongerFoot = mock(Menu.class);

		when(playerName.getValue()).thenReturn("Lionel Messi");
		when(leagueTitle.selectItem()).thenReturn(LeagueTitle.LL);
		when(strongerFoot.selectItem()).thenReturn(StrongerFoot.LEFTFOOT);

		PlayerPresenter playerPresenter = new PlayerPresenter(
				new PlayerActionView(playerName, strongerFoot, leagueTitle));
		Player striker = playerPresenter.getPlayer();
		assertThat(outContent.toString(), containsString("New Player Menu"));
		assertThat(striker.getName(), is("Lionel Messi"));
	}
}
