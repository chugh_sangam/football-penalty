package com.penalties.football.app.game.refree;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.game.strategy.InitialGameStrategy;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.utils.PlayerBuilderHelper;

public class RefereeInChargeTests {

	Player striker;
	Player goalKeeper;
	GameStrategy gameStrategy;
	Game game;

	@Before
	public void setUp() {
		striker = PlayerBuilderHelper.createStriker();
		goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		gameStrategy = mock(InitialGameStrategy.class);
	}

	@Test
	public void testStart() {
		Referee referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		game = referee.getGame();
		Mockito.doNothing().when(gameStrategy).start(striker, goalKeeper, game);
		referee.start();
		assertThat(referee.getStriker().getExperience().getGames().size(), is(1));
	}

	@Test
	public void testGetStriker() {
		Referee referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		assertThat(striker, is(referee.getStriker()));
	}

	@Test
	public void testGetGoalKeeper() {
		RefereeInCharge referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		assertThat(goalKeeper, is(referee.getGoalKeeper()));
	}

	@Test
	public void testGetBoutStatus() {
		GameStatus gameStatus = GameStatus.values()[0];
		when(gameStrategy.getGameStatus()).thenReturn(gameStatus);
		Referee referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		assertThat(referee.getGameStatus(), is(gameStatus));
	}
}
