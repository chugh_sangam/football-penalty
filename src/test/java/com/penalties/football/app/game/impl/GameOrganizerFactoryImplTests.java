package com.penalties.football.app.game.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.game.GameActionView;
import com.penalties.football.app.game.GameInitializer;
import com.penalties.football.app.game.GameNotificationView;
import com.penalties.football.app.game.GameOrganizer;
import com.penalties.football.app.game.GameOrganizerFactory;
import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.game.refree.RefereeInCharge;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.game.strategy.InitialGameStrategy;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.utils.PlayerBuilderHelper;

public class GameOrganizerFactoryImplTests {

	private Referee referee;

	private GameInitializer gameIntializer;

	private Player striker;

	private Player goalKeeper;

	@Before
	public void setUp() {

		Menu<ShotType> menu = new Menu<>("Choose your action:", ShotType.values());
		striker = PlayerBuilderHelper.createStriker();
		goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		GameStrategy gameStrategy = new InitialGameStrategy(new GameActionView(menu));
		referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		gameIntializer = mock(GameIntializerImpl.class);
	}

	@Test
	public void testCreate() {
		when(gameIntializer.create()).thenReturn(Optional.of(referee));
		GameOrganizerFactory gameOrganizerFactory = new GameOrganizerFactoryImpl(gameIntializer,
				new GameNotificationView());
		Optional<GameOrganizer> gameOrganizer = gameOrganizerFactory.create();
		assertThat(gameOrganizer.isPresent(), is(true));
	}

	@Test
	public void testLoad() {
		when(gameIntializer.load()).thenReturn(Optional.of(referee));
		GameOrganizerFactory gameOrganizerFactory = new GameOrganizerFactoryImpl(gameIntializer,
				new GameNotificationView());
		Optional<GameOrganizer> gameOrganizer = gameOrganizerFactory.load();
		assertThat(gameOrganizer.isPresent(), is(true));
	}

	@Test
	public void testGetStriker() {
		when(gameIntializer.getStriker()).thenReturn(Optional.of(striker));
		GameOrganizerFactory gameOrganizerFactory = new GameOrganizerFactoryImpl(gameIntializer,
				new GameNotificationView());
		Optional<Player> strikerPlayer = gameOrganizerFactory.getStriker();
		assertThat(strikerPlayer.isPresent(), is(true));
	}

	@Test
	public void testOrganizeGameForProfile() {
		when(gameIntializer.findNewGoalKeeper()).thenReturn(Optional.of(referee));
		GameOrganizerFactory gameOrganizerFactory = new GameOrganizerFactoryImpl(gameIntializer,
				new GameNotificationView());
		Optional<GameOrganizer> gameOrganizer = gameOrganizerFactory.organizeGameForProfile();
		assertThat(gameOrganizer.isPresent(), is(true));
	}

	@Test
	public void testOrganizeGameForProfileShouldReturnNullWhenNoReferee() {
		when(gameIntializer.findNewGoalKeeper()).thenReturn(Optional.empty());
		GameOrganizerFactory gameOrganizerFactory = new GameOrganizerFactoryImpl(gameIntializer,
				new GameNotificationView());
		Optional<GameOrganizer> gameOrganizer = gameOrganizerFactory.organizeGameForProfile();
		assertThat(gameOrganizer.isPresent(), is(false));
	}

}