package com.penalties.football.app.game.strategy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.game.GameActionView;
import com.penalties.football.app.model.Game;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.utils.GameBuilderHelper;
import com.penalties.football.app.utils.PlayerBuilderHelper;

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest({ InitialGameStrategy.class })
public class InitialGameStrategyTests {

	private Player striker;

	private Player goalKeeper;

	private Game game;

	private Menu<ShotType> menu;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {
		menu = mock(Menu.class);
		game = GameBuilderHelper.getGame();
		striker = PlayerBuilderHelper.createStriker();
		goalKeeper = PlayerBuilderHelper.createGoalKeeper();
	}

	@Test
	public void testStart() throws Exception {
		when(menu.selectItem()).thenReturn(ShotType.LLC);
		GameActionView gameActionView = mock(GameActionView.class);

		PowerMockito.whenNew(GameActionView.class).withArguments(menu).thenReturn(gameActionView);

		InitialGameStrategy spy = PowerMockito.spy(new InitialGameStrategy(gameActionView));
		spy.start(striker, goalKeeper, game);
		GameStatus gameStatus = spy.getGameStatus();
		assertThat(gameStatus, is(GameStatus.COMPLETED));
		PowerMockito.verifyPrivate(spy, times(1)).invoke("startGame");
		PowerMockito.verifyPrivate(spy, times(1)).invoke("nextIteration");

	}

	@Test
	public void testOnShotSelection() throws Exception {
		when(menu.selectItem()).thenReturn(ShotType.LLC);
		GameActionView gameActionView = mock(GameActionView.class);

		PowerMockito.whenNew(GameActionView.class).withArguments(menu).thenReturn(gameActionView);

		InitialGameStrategy spy = PowerMockito.spy(new InitialGameStrategy(gameActionView));
		spy.start(striker, goalKeeper, game);
		spy.onShotSelection(ShotType.LLC);
		GameStatus gameStatus = spy.getGameStatus();
		assertThat(gameStatus, is(GameStatus.COMPLETED));
		PowerMockito.verifyPrivate(spy, times(1)).invoke("getGoalKeeperReaction");
		PowerMockito.verifyPrivate(spy, times(2)).invoke("nextIteration");
	}

	@Test
	public void testOnSaveAndExit() throws Exception {
		when(menu.selectItem()).thenReturn(ShotType.LLC);
		GameActionView gameActionView = mock(GameActionView.class);

		PowerMockito.whenNew(GameActionView.class).withArguments(menu).thenReturn(gameActionView);

		InitialGameStrategy spy = PowerMockito.spy(new InitialGameStrategy(gameActionView));
		spy.start(striker, goalKeeper, game);
		spy.onSaveAndExit();
		GameStatus gameStatus = spy.getGameStatus();
		assertThat(gameStatus, is(GameStatus.SAVED));
		PowerMockito.verifyPrivate(spy, times(1)).invoke("startGame");
		PowerMockito.verifyPrivate(spy, times(1)).invoke("nextIteration");
	}

}
