package com.penalties.football.app.game;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.game.refree.RefereeInCharge;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.game.strategy.InitialGameStrategy;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.NotificationView;
import com.penalties.football.app.utils.PlayerBuilderHelper;
import com.penalties.football.app.utils.PrintStreamHelper;

public class GameNotificationViewTests extends PrintStreamHelper {

	Player striker;
	Player goalKeeper;
	GameStrategy gameStrategy;

	@Before
	public void setUp() {
		striker = PlayerBuilderHelper.createStriker();
		goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		gameStrategy = mock(InitialGameStrategy.class);
	}

	@Test
	public void testShowWinnerNotification() {
		Referee referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		referee.start();
		referee.getGame().setGoalScored(3);
		referee.getGame().setGoalDefended(2);
		NotificationView notificationView = new GameNotificationView();
		notificationView.showWinnerNotification(referee);
		assertThat(outContent.toString(), startsWith("The Striker"));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testDrawShouldThrowUnsupportedOperationException() {
		NotificationView notificationView = new GameNotificationView();
		notificationView.draw();
		assertThat(outContent.toString(), containsString("UnsupportedOperationException"));
	}

	@Test
	public void testShowPausedNotification() {
		NotificationView notificationView = new GameNotificationView();
		notificationView.showPausedNotification();
		assertThat(outContent.toString(), startsWith("You have selected to pause this game"));
	}

}
