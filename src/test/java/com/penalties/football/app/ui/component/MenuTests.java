package com.penalties.football.app.ui.component;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.penalties.football.app.constant.CommonEnum.GameStatus;
import com.penalties.football.app.utils.PrintStreamHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Menu.class)
public class MenuTests extends PrintStreamHelper {

	@Test
	public void testDrawShouldPrintItemsWhenItemsListSizeGreaterThanZero() {
		Menu<GameStatus> menu = new Menu<>("Game Status", GameStatus.values());
		menu.draw();
		assertThat(outContent.toString(), containsString("Game Status"));
		assertThat(outContent.toString(), containsString(GameStatus.COMPLETED.toString()));

		PrintStream originalOut = System.out;
		System.setOut(originalOut);
	}

	@Test
	public void testchooseItemShouldReturnFirstItem() throws Exception {
		Menu<GameStatus> menu = PowerMockito.spy(new Menu<>("Fight Status", GameStatus.values()));
		PowerMockito.doReturn(0).when(menu, "readItemIndex");
		GameStatus fightStatus = menu.selectItem();
		assertThat(fightStatus, is(GameStatus.IN_PROGRESS));
	}

}
