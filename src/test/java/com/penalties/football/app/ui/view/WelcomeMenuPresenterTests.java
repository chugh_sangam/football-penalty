package com.penalties.football.app.ui.view;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.penalties.football.app.game.GameOrganizer;
import com.penalties.football.app.game.GameOrganizerFactory;
import com.penalties.football.app.game.impl.GameOrganizerFactoryImpl;
import com.penalties.football.app.game.impl.GameOrganizerImpl;
import com.penalties.football.app.ui.component.Menu;
import com.penalties.football.app.ui.view.WelcomeMenuConsoleView.MainMenuItem;
import com.penalties.football.app.utils.PrintStreamHelper;

public class WelcomeMenuPresenterTests extends PrintStreamHelper {

	private Menu<MainMenuItem> menu;
	private WelcomeMenuConsoleView welcomeMenuConsoleView;
	private GameOrganizer gameOrganizer;
	private GameOrganizerFactory gameOrganizerFactory;
	private WelcomeMenuPresenter menuPresenter;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {
		this.menu = mock(Menu.class);
		this.welcomeMenuConsoleView = new WelcomeMenuConsoleView(menu);
		this.gameOrganizerFactory = mock(GameOrganizerFactoryImpl.class);
		this.gameOrganizer = mock(GameOrganizerImpl.class);
		this.menuPresenter = new WelcomeMenuPresenter(welcomeMenuConsoleView, gameOrganizerFactory);
	}

	@Test
	public void testOnStartChosenShouldStartGameWhenBoutOrganizerFactoryReturnNonNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.START_NEW);
		when(gameOrganizerFactory.create()).thenReturn(Optional.of(gameOrganizer));
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onStartChosen();
	}

	@Test
	public void testOnStartChosenShouldNotStartGameWhenBoutOrganizerFactoryReturnNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.START_NEW);
		when(gameOrganizerFactory.create()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onStartChosen();
		String actual = outContent.toString();
		assertThat(actual, containsString("Sorry could not create game."));
	}

	@Test
	public void testOnResumeChosenShouldStartGameWhenBoutOrganizerFactoryReturnNonNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.START_NEW);
		when(gameOrganizerFactory.load()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onResumeChosen();

	}

	@Test
	public void testOnResumeChosenShouldNotStartGameWhenBoutOrganizerFactoryReturnNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.START_NEW);
		when(gameOrganizerFactory.load()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onResumeChosen();
		String actual = outContent.toString();
		assertThat(actual, containsString("Sorry could not load the previous game at this time."));
	}

	@Test
	public void testOnviewProfileChosenShouldStartGameWhenBoutOrganizerFactoryReturnNonNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.VIEW_PLAYER_PROFILE);
		when(gameOrganizerFactory.getStriker()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onviewProfileChosen();
	}

	@Test
	public void testOnviewProfileChosenShouldNotStartGameWhenBoutOrganizerFactoryReturnNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.VIEW_PLAYER_PROFILE);
		when(gameOrganizerFactory.getStriker()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onviewProfileChosen();
		String actual = outContent.toString();
		assertThat(actual, containsString("Profile does not exist"));
	}

	@Test
	public void testOnContiWithExistingProfileChosenShouldStartGameWhenBoutOrganizerFactoryReturnNonNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.VIEW_PLAYER_PROFILE);
		when(gameOrganizerFactory.organizeGameForProfile()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onContiWithExistingProfileChosen();
	}

	@Test
	public void testOnContiWithExistingProfileChosenShouldNotStartGameWhenBoutOrganizerFactoryReturnNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.VIEW_PLAYER_PROFILE);
		when(gameOrganizerFactory.organizeGameForProfile()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onContiWithExistingProfileChosen();
		String actual = outContent.toString();
		assertThat(actual, containsString("Sorry, We cannot find any profile for now"));
	}

	@Test
	public void testOnExitChosenShouldNotStartGameWhenBoutOrganizerFactoryReturnNull() {
		when(this.menu.selectItem()).thenReturn(MainMenuItem.VIEW_PLAYER_PROFILE);
		when(gameOrganizerFactory.organizeGameForProfile()).thenReturn(Optional.empty());
		doNothing().when(gameOrganizer).start();
		menuPresenter = spy(menuPresenter);
		doNothing().when(menuPresenter).show();
		menuPresenter.onExitChosen();
		String actual = outContent.toString();
		assertThat(actual, startsWith("Thanks for playing the Football Penalty"));
	}

}
