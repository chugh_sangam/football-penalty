/**
 * 
 */
package com.penalties.football.app.utils;

import java.util.ArrayList;
import java.util.List;

import com.penalties.football.app.constant.CommonEnum.GoalKeeper;
import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.constant.CommonEnum.PlayerLevel;
import com.penalties.football.app.constant.CommonEnum.PlayerType;
import com.penalties.football.app.constant.CommonEnum.StrongerFoot;
import com.penalties.football.app.model.Player;

/**
 * @author user
 *
 */
public class PlayerBuilderHelper {

	public static Player createStriker() {
		Player striker = new Player("Lionel Messi", PlayerLevel.Beginner, PlayerType.STRIKER, LeagueTitle.LL,
				StrongerFoot.LEFTFOOT);
		return striker;
	}

	public static Player createGoalKeeper() {
		Player goalKeeper = new Player(GoalKeeper.IKER_CASILLAS.toString(), PlayerLevel.Professional,
				PlayerType.GOALKEEPER, null, null);
		return goalKeeper;
	}

	public static List<Player> createGoalKeeperList() {
		Player goalKeeper = new Player(GoalKeeper.IKER_CASILLAS.toString(), PlayerLevel.Professional,
				PlayerType.GOALKEEPER, null, null);
		List<Player> goalKeepers = new ArrayList<>();
		goalKeepers.add(goalKeeper);
		return goalKeepers;
	}

}
