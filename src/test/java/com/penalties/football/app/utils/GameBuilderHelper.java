package com.penalties.football.app.utils;

import java.time.LocalDateTime;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.model.Game;

public class GameBuilderHelper {

	public static Game getGame() {
		Game game = new Game(LocalDateTime.now(), LeagueTitle.LL, CommonConstant.SHOT_START_COUNTER);
		return game;
	}
}
