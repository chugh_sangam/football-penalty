/**
 * 
 */
package com.penalties.football.app.utils;

import com.penalties.football.app.constant.CommonEnum.ShotType;
import com.penalties.football.app.game.GameActionView;
import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.game.refree.RefereeInCharge;
import com.penalties.football.app.game.strategy.GameStrategy;
import com.penalties.football.app.game.strategy.InitialGameStrategy;
import com.penalties.football.app.model.Player;
import com.penalties.football.app.ui.component.Menu;

/**
 * @author user
 *
 */
public class RefereeBuilderHelper {

	public static Referee createReferee() {
		Player striker = PlayerBuilderHelper.createStriker();
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();

		final Menu<ShotType> shotSelections = new Menu<>("Please do the shot selection : ", ShotType.values());
		GameStrategy gameStrategy = new InitialGameStrategy(new GameActionView(shotSelections));

		Referee referee = new RefereeInCharge(striker, goalKeeper, gameStrategy);
		return referee;
	}

}
