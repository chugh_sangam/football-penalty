package com.penalties.football.app.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.PlayerLevel;
import com.penalties.football.app.constant.CommonEnum.PlayerType;
import com.penalties.football.app.game.refree.Referee;
import com.penalties.football.app.utils.RefereeBuilderHelper;

public class PlayerTests {

	Player striker;
	Player goalKeeper;
	Game game;
	Referee referee;

	@Before
	public void setUp() {
		referee = RefereeBuilderHelper.createReferee();
		striker = referee.getStriker();
	}

	@Test
	public void testGetPlayerLevel() throws Exception {
		assertThat(striker.getPlayerLevel(), is(PlayerLevel.Beginner));

		game = new Game(LocalDateTime.now(), striker.getLeagueTitle(), CommonConstant.SHOT_START_COUNTER);
		striker.getExperience().getGames().add(game);
		game = new Game(LocalDateTime.now(), striker.getLeagueTitle(), CommonConstant.SHOT_START_COUNTER);
		striker.getExperience().getGames().add(game);

		this.referee.setStrikerExperience(striker);
		assertThat(striker.getPlayerLevel(), is(PlayerLevel.Amatuer));
	}

	@Test
	public void testGetPlayerTypeAsStriker() {
		striker = referee.getStriker();
		assertThat(striker.getPlayerType(), is(PlayerType.STRIKER));
	}

	@Test
	public void testGetPlayerTypeAsGoalKeeper() {
		goalKeeper = referee.getGoalKeeper();
		assertThat(goalKeeper.getPlayerType(), is(PlayerType.GOALKEEPER));
	}
}
