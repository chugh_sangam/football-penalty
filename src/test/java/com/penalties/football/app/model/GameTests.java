package com.penalties.football.app.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import com.penalties.football.app.constant.CommonConstant;
import com.penalties.football.app.constant.CommonEnum.LeagueTitle;
import com.penalties.football.app.utils.PlayerBuilderHelper;

public class GameTests {

	Game game = null;

	@Before
	public void setup() {
		game = new Game(LocalDateTime.now(), LeagueTitle.LL, CommonConstant.SHOT_START_COUNTER);
	}

	@Test
	public void testCanPlay() {
		assertThat(game.canPlay(), is(true));
	}

	@Test
	public void testCanPlayShouldReturnFalseForValueGreaterThanMaxCounter() {
		game.setShotNumber(7);
		assertThat(game.canPlay(), is(false));
	}

	@Test
	public void testCanPlayShouldReturnFalseForNegativeValue() {
		game.setShotNumber(-2);
		assertThat(game.canPlay(), is(false));
	}

	@Test
	public void testGetLeagueTitle() {
		assertThat(game.getLeagueTitle(), is(LeagueTitle.LL));
		game.setLeagueTitle(LeagueTitle.PL);
		assertThat(game.getLeagueTitle(), is(LeagueTitle.PL));
	}

	@Test
	public void testGetWinnerAsStriker() {
		Player striker = PlayerBuilderHelper.createStriker();
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		game.setGoalDefended(2);
		game.setGoalScored(3);
		game.setResult(striker, goalKeeper);
		assertThat(game.getWinner(), is(striker.getName()));
	}

	@Test
	public void testGetWinnerAsGoalKeeper() {
		Player striker = PlayerBuilderHelper.createStriker();
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		game.setGoalDefended(4);
		game.setGoalScored(1);
		game.setResult(striker, goalKeeper);
		assertThat(game.getWinner(), is(goalKeeper.getName()));
	}

	@Test
	public void testGetRunnerUpAsStriker() {
		Player striker = PlayerBuilderHelper.createStriker();
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		game.setGoalDefended(4);
		game.setGoalScored(1);
		game.setResult(striker, goalKeeper);
		assertThat(game.getRunnerup(), is(striker.getName()));
	}

	@Test
	public void testGetRunnerUpAsGoalKeeper() {
		Player striker = PlayerBuilderHelper.createStriker();
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		game.setGoalDefended(2);
		game.setGoalScored(3);
		game.setResult(striker, goalKeeper);
		assertThat(game.getRunnerup(), is(goalKeeper.getName()));
	}

	@Test
	public void testGetRunnerUpAsStrikerShouldFailWhenMoreGoalScored() {
		Player striker = PlayerBuilderHelper.createStriker();
		Player goalKeeper = PlayerBuilderHelper.createGoalKeeper();
		game.setGoalDefended(2);
		game.setGoalScored(3);
		game.setResult(striker, goalKeeper);
		assertThat(game.getRunnerup(), is(striker.getName()));
	}

}
